package home.rao.catalog.dao;

import home.rao.catalog.domain.Product;

import java.util.Optional;

public interface ProductDao extends Dao<Product> {

    Optional<Product> findByName(String name);

    Boolean updateDescription(Product product);
}
