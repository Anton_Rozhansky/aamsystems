package home.rao.catalog.dao.impl;

import home.rao.catalog.dao.ProductDao;
import home.rao.catalog.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.List;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class ProductDaoImpl implements ProductDao {

    private final JdbcTemplate jdbcTemplate;

    private final static String TABLE = "product";
    private final static String SELECT_ALL = "SELECT * FROM " + TABLE;
    private final static String SELECT_BY_ID = SELECT_ALL + " WHERE id = ?";
    private final static String SELECT_BY_NAME = SELECT_ALL + " WHERE name = ?";
    private final static String UPDATE_DESCRIPTION = "UPDATE " + TABLE + " SET description = ? WHERE id = ?";
    private final static String DELETE = "DELETE FROM " + TABLE + " WHERE id = ?";


    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SELECT_ALL, new BeanPropertyRowMapper<>(Product.class));
    }

    @Override
    public Optional<Product> findById(Long id) {
        return jdbcTemplate.query(SELECT_BY_ID, new BeanPropertyRowMapper<>(Product.class), id)
                .stream()
                .findFirst();
    }

    @Override
    public Optional<Product> findByName(String name) {
        return jdbcTemplate.query(SELECT_BY_NAME, new BeanPropertyRowMapper<>(Product.class), name)
                .stream()
                .findFirst();
    }

    @Override
    public Long save(Product product) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName(TABLE)
                .usingGeneratedKeyColumns(Product.Fields.id);
        return jdbcInsert.executeAndReturnKey(mapToRow(product)).longValue();
    }

    @Override
    public Boolean updateDescription(Product product) {
        return jdbcTemplate.update(UPDATE_DESCRIPTION, product.getDescription(), product.getId()) == 1;
    }

    @Override
    public Boolean delete(Long id) {
        return jdbcTemplate.update(DELETE, id) == 1;
    }

    private SqlParameterSource mapToRow(Product product) {
        return new MapSqlParameterSource()
                .addValue(Product.Fields.name, product.getName(), Types.VARCHAR)
                .addValue(Product.Fields.description, product.getDescription(), Types.VARCHAR);
    }
}
