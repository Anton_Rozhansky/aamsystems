package home.rao.catalog.dao;

import java.util.List;
import java.util.Optional;

public interface Dao<T> {

    List<T> findAll();

    Optional<T> findById(Long id);

    Long save(T t);

    Boolean delete(Long id);
}
