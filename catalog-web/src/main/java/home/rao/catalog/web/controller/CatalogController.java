package home.rao.catalog.web.controller;

import home.rao.catalog.domain.Product;
import home.rao.catalog.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/products")
public class CatalogController {

    private final ProductService productService;

    @GetMapping()
    public String listProducts(Model theModel){
        List<Product> products = productService.getAll();
        theModel.addAttribute("products", products);
        return "list-products";
    }

    @GetMapping("/add")
    public String showFormForAdd(Model theModel) {
        Product newProduct = new Product();
        theModel.addAttribute("product", newProduct);
        return "add-product";
    }

    @PostMapping("/saveProduct")
    public String saveProduct(@ModelAttribute("product") Product product) {
        productService.create(product);
        return "redirect:/products";
    }

    @GetMapping("/update")
    public String showFormForUpdate(@RequestParam("productId") Long theId,
                                    Model theModel) {

        Product product = productService.getById(theId);
        theModel.addAttribute("product", product);
        return "update-product";
    }

    @PostMapping("/{id}")
    public String updateProduct(@PathVariable long id,
                                @ModelAttribute("product") Product product) {
        productService.update(id, product);
        return "redirect:/products";
    }

    @GetMapping("/delete")
    public String deleteCustomer(@RequestParam("productId") Long productId) {
        productService.delete(productId);
        return "redirect:/products";
    }
}
