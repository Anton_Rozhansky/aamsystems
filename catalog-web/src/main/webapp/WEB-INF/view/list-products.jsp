<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
    <title>List Products</title>
</head>
<body>
<div>
    <div>
        <h2>Products</h2>
    </div>
</div>
<div id="container">
    <div id="content">
        <input type="button" value="Add Product"
               onclick="window.location.href='products/add'; return false;"
               class="add-button"
        />
        <table>
            <tr>
                <th>Product</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
            <c:forEach var="product" items="${products}">
                <c:url var="updateLink" value="/products/update">
                    <c:param name="productId" value="${product.id}"/>
                </c:url>
                <c:url var="deleteLink" value="/products/delete">
                    <c:param name="productId" value="${product.id}"/>
                </c:url>
                <tr>
                    <td> ${product.name} </td>
                    <td> ${product.description} </td>
                    <td>
                        <a href="${updateLink}">Update</a>
                        |
                        <a href="${deleteLink}"
                           onclick="if (!(confirm('Are you sure you want to delete this product?'))) return false">Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
</body>
</html>









