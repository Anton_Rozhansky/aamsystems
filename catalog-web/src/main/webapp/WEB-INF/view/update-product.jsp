<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
	<title>Update Product</title>
</head>
<body>
	<div id="container">
		<h3>Save Product</h3>
		<form:form action="${product.id}" modelAttribute="product" method="POST">
			<table>
				<tbody>
					<tr>
						<td><label>Product:</label></td>
						<td><label>${product.name}</label></td>
					</tr>
					<tr>
						<td><label>Description:</label></td>
						<td><form:input path="description" /></td>
					</tr>
					<tr>
						<td><label></label></td>
						<td><input type="submit" value="Save"/></td>
					</tr>
				</tbody>
			</table>
		</form:form>
		<div></div>
		<p>
			<a href="${pageContext.request.contextPath}/products">Back to List</a>
		</p>
	</div>
</body>
</html>










