package home.rao.catalog.service;

import home.rao.catalog.domain.Product;

public interface ProductService extends GenericService<Product> {

    Product getByName(String name);
}
