package home.rao.catalog.service.impl;

import home.rao.catalog.dao.ProductDao;
import home.rao.catalog.domain.Product;
import home.rao.catalog.service.ProductService;
import home.rao.catalog.service.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductDao productDao;

    @Override
    @Transactional(readOnly = true)
    public List<Product> getAll() {
        log.debug("getAll - start");

        List<Product> products = productDao.findAll();

        log.debug("getAll - end: products = {}", products);
        return products;
    }

    @Override
    @Transactional(readOnly = true)
    public Product getById(Long id) {
        log.debug("getById - start: id = {}", id);

        Product product = productDao.findById(id)
                .orElseThrow(() -> new NotFoundException("No product with id '" + id + "' was found"));

        log.debug("getById - end: product = {}", product);
        return product;
    }

    @Override
    @Transactional(readOnly = true)
    public Product getByName(String name) {
        log.debug("getByName - start: name = {}", name);

        Product product = productDao.findByName(name)
                .orElseThrow(() -> new NotFoundException("Product '" + name + "' can not be found"));

        log.debug("getByName - end: product = {}", product);
        return product;
    }

    @Override
    public Long create(Product product) {
        log.debug("create - start: product = {}", product);

        Long id = productDao.save(product);

        log.debug("create - end: id = {}", id);
        return id;
    }

    @Override
    public Boolean update(Long id, Product product) {
        log.debug("update - start: product = {}", product);

        getById(id);
        Product update = new Product()
                .setId(id)
                .setDescription(product.getDescription());
        Boolean result = productDao.updateDescription(update);

        log.debug("update - end: result = {}", result);
        return result;
    }

    @Override
    public Boolean delete(Long id) {
        log.debug("delete - start: id = {}", id);

        Boolean result = productDao.delete(id);

        log.debug("delete - end: result = {}", result);
        return result;
    }
}
