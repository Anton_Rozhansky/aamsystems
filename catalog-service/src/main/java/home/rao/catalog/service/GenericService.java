package home.rao.catalog.service;

import java.util.List;

public interface GenericService<T> {

    List<T> getAll();

    T getById(Long id);

    Long create(T t);

    Boolean update(Long id, T t);

    Boolean delete(Long id);
}
